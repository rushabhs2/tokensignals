from django.apps import AppConfig


class TokensignapiConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'tokensignapi'
