from django.contrib import admin
from django.urls import path, include
from tokensignapi import views
from rest_framework.routers import DefaultRouter

# for Auth token 
from django.conf import settings
from django.db.models.signals import post_save
from django.dispatch import receiver
from rest_framework.authtoken.models import Token


# creating Router Object
router = DefaultRouter()

# Register StudentViewSet with Router
router.register('studentapi', views.StudentModelViewSet, basename='student' )

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include(router.urls)),     
    path('auth/', include('rest_framework.urls', namespace='rest_framework')),

]

# this signal creates Auth Token for Users 
@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def create_auth_token(sender, instance=None, created=False, **kwargs):
    if created:
        Token.objects.create(user=instance)